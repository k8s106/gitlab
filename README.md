# Deploy gitlab-ce in kubernetes
 

## Getting started

This's instruction usage guide [install gitlab using docker compose](https://docs.gitlab.com/ee/install/docker.html#install-gitlab-using-docker-compose).

He was adapted under k8s manifests. 


## Install

if you want check kustomize build yaml

```shell
kustomize build kustomize/overlays/gitlab > gitlab-build.yaml
```

Run kustomize

```shell
kubectl create namespace gitlab
kustomize build kustomize/overlays/gitlab | kubectl apply -f -
```


## Exposing ssh port ingress

Default [ingress does not support TCP or UDP services](https://kubernetes.github.io/ingress-nginx/user-guide/exposing-tcp-udp-services/) 

Check exist args: *--tcp-services-configmap=$(POD_NAMESPACE)/tcp-services* and *- --udp-services-configmap=$(POD_NAMESPACE)/udp-services*

```shell
# Check exist args for deployment 
kubectl get deploy/ingress-nginx-controller  -n ingress-nginx -o yaml
```

```shell
# Check exist args for daemonset 
kubectl get daemonset.apps/ingress-nginx-controller  -n ingress-nginx -o yaml
```

Add exposing ssh port

```shell
kubectl patch configmap tcp-services -n ingress-nginx  --patch-file ./patch/patch-ingress-config-map.yaml
```



## Links

- [kustomize](https://github.com/kubernetes-sigs/kustomize)
- [kustomize resources](https://kustomize.io/#resources)
- [Change base YAML config for different environments prod/test using Kustomize](https://levelup.gitconnected.com/kubernetes-change-base-yaml-config-for-different-environments-prod-test-6224bfb6cdd6)
- [Kustomize — The right way to do templating in Kubernetes](https://blog.stack-labs.com/code/kustomize-101/)
- [install gitlab using docker compose](https://docs.gitlab.com/ee/install/docker.html#install-gitlab-using-docker-compose).
